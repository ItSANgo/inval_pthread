/**
 * @file main.c
 * @author Mitsutoshi Nakano <ItSANgo@gmail.com>
 * @brief 
 * @version 0.1.0
 * @date 2020-07-04
 * 
 * Copyright (c) 2020 Mitsutoshi Nakano <ItSANgo@gmail.com>
 * 
 */

#include "body.h"

int
main(int argc, char *argv[])
{
	return body(argc, argv);
}
