#! /usr/bin/make -f
TARGETS=$(DEPS) $(PROGRAMS)
PROGRAMS=inval
HEADERS=body.h
inval_SRCS=main.c body.c
inval_OBJS=$(inval_SRCS:.c=.o)
SRCS=$(inval_SRCS)
OBJS=$(inval_OBJS)
DEPS=.depend
CFLAGS=-Wall -g
LDLIBS=-lpthread -lbsd
.PHONY: all depend clean
all: $(TARGETS)
-include $(DEPS)
inval: $(inval_OBJS)
	$(CC) -o $@ $(LDFLAGS) $(inval_OBJS) $(LDLIBS)
depend: $(DEPS)
$(DEPS): $(SRCS) $(HEADERS)
	$(CC) -M -o $(DEPS) $(SRCS)
clean:
	$(RM) $(TARGETS) $(OBJS) core core.* core-*
