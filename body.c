/**
 * @file body.c
 * @author Mitsutoshi Nakano <ItSANgo@gmail.com>
 * @brief 
 * @version 0.1.0
 * @date 2020-07-04
 * 
 * Copyright (c) 2020 Mitsutoshi Nakano <ItSANgo@gmail.com>
 * 
 */

#include <stdio.h>
#include <limits.h>
#include <signal.h>
#include <pthread.h>
#include <sysexits.h>
#include <bsd/err.h>
#include "body.h"

enum { LOOP_COUNTER = 20 };

int
kill_wrong_thread(int argc, char *argv[])
{
	pthread_t th =0x7f4000659700;
	int err = pthread_kill(th, 0); // FIXME: SEGV!
	if (err) {
		warnc(err, "pthread_kill(%lu, 0)", th);
	}
	return EX_OK;
}

void *
exit_soon_routine(void *_)
{
	printf("exiting..\n");
	pthread_exit(NULL);
}

int
kill_a_thread(pthread_t th, int sig)
{
	int err = pthread_kill(th, sig);
	if (err) {
		warnc(err, "pthread_kill(%lx, %d)", th, sig);
	}
	return err;
}

int
join_a_thread(pthread_t th, void **ret)
{
	int join_err = pthread_join(th, ret);
	if (join_err) {
		warnc(join_err, "pthraed_join(%lx, %p)", th, ret);
	}
	return join_err;
}

int
kill_exited_thread(int argc, char *argv[])
{
	pthread_t th;
	int err = pthread_create(&th, NULL, exit_soon_routine, NULL);
	if (err) {
		errc(EX_OSERR, err, "pthread_create()");
	}
	int i;
	for (i = 0; i < LOOP_COUNTER; ++i) {
		printf("check %d\n", i);
		kill_a_thread(th, 0);
	}
	int j;
	for (j = 0; j < LOOP_COUNTER; ++j) {
		printf("cancel %d\n", j);
		int cancel_err = pthread_cancel(th);
		if (cancel_err) {
			warnc(cancel_err, "pthread_cancel(%lx)", th);
		}
	}
	kill_a_thread(th, 0);
	join_a_thread(th, NULL);
	join_a_thread(th, NULL);
	kill_a_thread(th, 0);
	return EX_OK;
}

int
body(int argc, char *argv[])
{
	// return kill_wrong_thread(argc, argv);
	return kill_exited_thread(argc, argv);
}
