/**
 * @file body.h
 * @author Mitsutoshi Nakano <ItSANgo@gmail.com>
 * @brief 
 * @version 0.1.0
 * @date 2020-07-04
 * 
 * Copyright (c) 2020 Mitsutoshi Nakano <ItSANgo@gmail.com>
 * 
 */
#ifndef BODY_H
#define BODY_H

int body(int argc, char *argv[]);

#endif // BODY_H